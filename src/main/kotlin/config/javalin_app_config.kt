package config

import constants.ApplicationConstants
import io.javalin.Javalin
import io.javalin.JavalinEvent
import org.koin.core.KoinProperties
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.getProperty
import org.koin.standalone.inject

class JavalinAppConfig : KoinComponent {

    private val routeConfig: RouteConfig by inject()

    fun init(): Javalin {

        startKoin(
            listOf(KoinModuleConfig.applicationModule),
            KoinProperties(useEnvironmentProperties = true, useKoinPropertiesFile = true)
        )




        // TODO: Verificar utilização de properties
        DatabaseFactory.init(
            "jdbc:postgresql://localhost:5432/dojosample",
            "org.postgresql.Driver",
            "postgres",
            "concrete"
        )

        val app = Javalin.create().apply {
            exception(Exception::class.java) { e, _ -> e.printStackTrace() }
            error(404) { ctx -> ctx.json("not found") }
            port(getProperty(ApplicationConstants.SERVER_PORT, ApplicationConstants.DEFAULT_SERVER_PORT))
        }.event(JavalinEvent.SERVER_STOPPING) {
            stopKoin()
        }.start()

        routeConfig.register(app)

        return app
    }
}

fun main(args: Array<String>) {
    JavalinAppConfig().init()
}
package exceptions

class NotFoundRateException(msg: String) : Exception(msg)

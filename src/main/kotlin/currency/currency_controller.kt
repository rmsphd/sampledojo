package currency

import exceptions.CreateCurrencyException
import exceptions.ValidationException
import io.javalin.Context
import org.eclipse.jetty.http.HttpStatus


class CurrencyController(private val service: CurrencyService) {


    fun findAll(ctx: Context) {
        val currencies = service.findAll()
        if (currencies.isEmpty()) {
            ctx.status(HttpStatus.NOT_FOUND_404)
            return
        }
        ctx.json(currencies).status(HttpStatus.OK_200)

    }

    fun create(ctx: Context) {
        try {
            val currencies = service.create(ctx.body<Currency>())
            ctx.json(currencies).status(HttpStatus.CREATED_201)
        } catch (createException: CreateCurrencyException) {
            ctx.json(createException).status(HttpStatus.BAD_REQUEST_400)
        } catch (validationException: ValidationException) {
            ctx.json(validationException).status(HttpStatus.UNPROCESSABLE_ENTITY_422)
        }
    }
}
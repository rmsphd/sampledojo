package currency

import exceptions.CreateCurrencyException
import exceptions.ValidationException

class CurrencyServiceImpl(private val dao: CurrencyDao) : CurrencyService {
    fun validate(currency: Currency) {
        if (currency.name.isBlank() || currency.alias.isBlank()) {
            throw ValidationException("Estrutura de dados inválida")
        }
    }

    override fun create(currency: Currency): Currency {
        validate(currency)
        if (dao.getCurrency(currency.alias) != null) {
            throw CreateCurrencyException("Moeda já existente")
        }
        return dao.create(currency)
    }

    override fun findAll(): List<Currency> {
        return dao.findAll()
    }

}
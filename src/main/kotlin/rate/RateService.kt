package rate

interface RateService {

    fun findRate(rate: Rate): Double?

}

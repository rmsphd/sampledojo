package rate

import exceptions.EmptyRateException
import exceptions.NotFoundRateException
import io.javalin.Context
import org.eclipse.jetty.http.HttpStatus

class RateController(val rateService: RateService) {

    fun findRate(ctx: Context) {
        val inputCurrency = ctx.queryParam("currency_input")
        val outputCurrency = ctx.queryParam("currency_output")
        val rate = Rate(inputCurrency, outputCurrency)
        try {
            val rateResult = rateService.findRate(rate)
            rateResult?.let{
                ctx.json(it).status(HttpStatus.OK_200)
            }
        } catch (exception: EmptyRateException){
            ctx.json(exception).status(HttpStatus.UNPROCESSABLE_ENTITY_422)
        } catch (exception: NotFoundRateException) {
            ctx.json(exception).status(HttpStatus.NOT_FOUND_404)
        }

    }
}

package currency

import exceptions.CreateCurrencyException
import exceptions.ValidationException
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals


class CurrencyServiceTest {

    lateinit var currencyDaoMock: CurrencyDao

    @Before
    fun setup() {
        currencyDaoMock = mockk(relaxed = true)
    }

    @Test
    fun `deve retornar uma lista de currencies`() {
        val currencies = listOf(Currency("BRL", "REAL"), Currency("USD", "DOLLAR"))

        every { currencyDaoMock.findAll() }.returns(currencies)


        val currenciesResult = CurrencyServiceImpl(currencyDaoMock).findAll()

        assertEquals(currencies, currenciesResult)
    }

    @Test
    fun `dado uma moeda, cria e retorna com sucesso`() {
        val currency = Currency("BRL", "Real")

        every { currencyDaoMock.create(currency) } returns currency

        val currencyResponse = CurrencyServiceImpl(currencyDaoMock).create(currency)

        assertEquals(currencyResponse, currency)
    }

    @Test
    fun `dado uma moeda que já existe, retorna exception create currency`() {
        val currency = Currency("BRL", "Real")
        val exception = "Moeda já existente"

        every { currencyDaoMock.getCurrency(currency.alias) } returns currency

        every { currencyDaoMock.create(currency) } returns currency

        var exceptionReturned = CreateCurrencyException("null")

        try {
            CurrencyServiceImpl(currencyDaoMock).create(currency)
        } catch(ex : CreateCurrencyException) {
            exceptionReturned = ex
        } finally {
            assertEquals(exceptionReturned.message,exception)
        }
    }

    @Test
    fun `dado uma moeda sem nome, retorna exception validation`() {
        val currency = Currency("BRL", "")
        val exception = "Estrutura de dados inválida"

        every { currencyDaoMock.getCurrency(currency.alias) } returns null

        every { currencyDaoMock.create(currency) } returns currency

        var exceptionReturned = ValidationException("null")

        try {
            CurrencyServiceImpl(currencyDaoMock).create(currency)
        } catch(ex : ValidationException) {
            exceptionReturned = ex
        } finally {
            assertEquals(exceptionReturned.message,exception)
        }
    }
}
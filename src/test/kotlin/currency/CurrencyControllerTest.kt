package currency

import exceptions.CreateCurrencyException
import exceptions.ValidationException
import io.javalin.Context
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.eclipse.jetty.http.HttpStatus
import org.junit.Before
import org.junit.Test

class CurrencyControllerTest {


    lateinit var currencyServiceMock: CurrencyService
    lateinit var contextMock: Context

    @Before
    fun setup() {
        currencyServiceMock = mockk(relaxed = true)
        contextMock = mockk(relaxed = true)
    }

    @Test
    fun `deve retornar uma lista de currencies`() {
        val currencies = listOf(Currency("BRL", "REAL"), Currency("USD", "DOLLAR"))

        every { currencyServiceMock.findAll() }.returns(currencies)


        CurrencyController(currencyServiceMock).findAll(contextMock)

        verify { contextMock.json(currencies).status(HttpStatus.OK_200) }
    }

    @Test
    fun `deve retornar uma lista vazia de currencies`() {
        val currencies = emptyList<Currency>()

        every { currencyServiceMock.findAll() }.returns(currencies)


        CurrencyController(currencyServiceMock).findAll(contextMock)

        verify { contextMock.status(HttpStatus.NOT_FOUND_404) }
    }

    @Test
    fun `deve retornar um currency e status created`() {
        val currency= Currency("BRL", "REAL")

        every { currencyServiceMock.create(currency) }.returns(currency)
        every { contextMock.body<Currency>() }.returns(currency)

        CurrencyController(currencyServiceMock).create(contextMock)

        verify { contextMock.json(currency).status(HttpStatus.CREATED_201) }
    }

    @Test
    fun `dado uma moeda ja existente deve retornar status code 400 e um json contendo informacoes`() {
        val currency= Currency("BRL", "REAL")
        val exception = CreateCurrencyException("Moeda já existente no banco de dados")

        every { currencyServiceMock.create(currency) }.throws(exception)
        every { contextMock.body<Currency>() }.returns(currency)

        CurrencyController(currencyServiceMock).create(contextMock)

        verify { contextMock.json(exception).status(HttpStatus.BAD_REQUEST_400) }
    }

    @Test
    fun `dado uma moeda sem o nome deve retornar status code 422 e um json contendo informacoes`() {
        val currency= Currency(alias = "BRL",name = "")
        val exception = ValidationException("Moeda sem o nome definido.")

        every { currencyServiceMock.create(currency) }.throws(exception)
        every { contextMock.body<Currency>() }.returns(currency)

        CurrencyController(currencyServiceMock).create(contextMock)

        verify { contextMock.json(exception).status(HttpStatus.UNPROCESSABLE_ENTITY_422) }
    }
}
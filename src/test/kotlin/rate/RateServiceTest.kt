package rate

import exceptions.EmptyRateException
import exceptions.NotFoundRateException
import io.mockk.verify
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class RateServiceTest {

    lateinit var rateServiceImpl: RateService

    @Before
    fun setup() {
         rateServiceImpl = RateServiceImpl()
    }

    @Test
    fun `Dado um valor em dolar deve retornar o valor convertido para real`() {
        //given
        val rateIn = Rate("USD", "BRL")

        //when
        val result = rateServiceImpl.findRate(rateIn)

        //then
        assertEquals(3.68, result)
    }


    @Test(expected = NotFoundRateException::class)
    fun `Dado uma moeda de entrada que não existe deve retornar nulo`() {
        //given
        val rateIn = Rate("ROXEDA", "BRL")

        //when
        val result = rateServiceImpl.findRate(rateIn)
    }

    @Test(expected = NotFoundRateException::class)
    fun `Dado uma moeda de saída que não existe deve retornar nulo`() {
        //given
        val rateIn = Rate("USD", "ROXEDA")

        //when
        val result = rateServiceImpl.findRate(rateIn)
    }

    @Test(expected = NotFoundRateException::class)
    fun `Dado uma moeda de entrada e saída que não existem deve retornar nulo`() {
        //given
        val rateIn = Rate("BIRL", "ROXEDA")

        //when
        val result = rateServiceImpl.findRate(rateIn)
    }

    @Test(expected = EmptyRateException::class)
    fun `Dado uma moeda de entrada vazia deve lançar EmptyRateException` () {

        //given
        val rateIn = Rate("", "USD")

        //when
        rateServiceImpl.findRate(rateIn)
    }

    @Test(expected = EmptyRateException::class)
    fun `Dado uma moeda de saída vazia deve lançar EmptyRateException` () {

        //given
        val rateIn = Rate("BRL", "")

        //when
        rateServiceImpl.findRate(rateIn)
    }

    @Test(expected = EmptyRateException::class)
    fun `Dado uma moeda de entrada vazia e uma moeda de saída vazia deve lançar EmptyRateException` () {

        //given
        val rateIn = Rate("", "")

        //when
        rateServiceImpl.findRate(rateIn)
    }


}
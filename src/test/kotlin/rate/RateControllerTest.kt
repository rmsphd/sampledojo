package rate

import exceptions.EmptyRateException
import exceptions.NotFoundRateException
import io.javalin.Context
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.eclipse.jetty.http.HttpStatus
import org.junit.Before
import kotlin.test.Test

class RateControllerTest {

    lateinit var rateServiceMock: RateService
    lateinit var contextMock: Context

    @Before
    fun setup() {
        rateServiceMock = mockk(relaxed = true)
        contextMock = mockk(relaxed = true)
    }

    @Test
    fun `Quando informar uma moeda de origem e destino válido, retorna a cotação com sucesso`() {

        //given
        val rateIn = Rate("USD", "BRL")
        val rateOut = 3.60

        //when
        every { rateServiceMock.findRate(rateIn) }.returns(rateOut)

        every { contextMock.body<Rate>() }.returns(rateIn)

        RateController(rateServiceMock).findRate(contextMock)

        //then
        verify { contextMock.json(rateOut).status(HttpStatus.OK_200) }

    }


    @Test
    fun `Quando informar uma moeda de origem e destino vazias, lança uma exceção`() {
        val rateIn = Rate("", "")
        val exception = EmptyRateException("Moeda de origem ou destino estão vazias")

        every { rateServiceMock.findRate(rateIn) }.throws(exception)

        every { contextMock.queryParam("currency_input") }.returns("")
        every { contextMock.queryParam("currency_output") }.returns("BIRL")

        RateController(rateServiceMock).findRate(contextMock)

        verify { contextMock.json(exception).status(HttpStatus.UNPROCESSABLE_ENTITY_422) }
    }

    @Test
    fun `Quando informar uma moeda de origem e destino invalidas, lança uma exceção`() {
        val rateIn = Rate("RELAX", "BIRL")
        val exception = NotFoundRateException("Moeda de origem ou destino nao existentes")

        every { rateServiceMock.findRate(rateIn) }.throws(exception)

        every { contextMock.queryParam("currency_input") }.returns("RELAX")
        every { contextMock.queryParam("currency_output") }.returns("BIRL")

        RateController(rateServiceMock).findRate(contextMock)

        verify { contextMock.json(exception).status(HttpStatus.NOT_FOUND_404) }
    }

}
